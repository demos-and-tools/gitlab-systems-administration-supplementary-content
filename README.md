# GitLab Sys-Admin Course Supplementary Content

## Official Course Content
The GitLab Systems Administration course is [outlined here](https://university.gitlab.com/pages/system-admin-training)

Additionally, the official Labs are also [listed here](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/sysadminhandson/)

Original [Slide deck here](https://docs.google.com/presentation/d/1OfY3outHBTVwvjP3oRHcNHBTA4pILFWk8Kn19ycHqLA) _(private file)_

## Additional Labs I created
Markdown instructions for [8 additional Bonus Labs](https://gitlab.com/demos-and-tools/gitlab-systems-administration-supplementary-content/-/blob/main/bonus-labs.MD?ref_type=heads)

- Bonus Lab 1 (Module 2) - Install Gitlab Manually
- Bonus Lab 2 (Module 2) - Configure and test Postfix Email
- Bonus Lab 3 (Module 2) - Lab use Docker executor with Runner
- Bonus Lab 4 (Module 3) - ⁠Lab Upgrade Gitlab from 16 to 17.1
- Bonus Lab 5 (Module 3) - ⁠Lab Downgrade Gitlab back to 16.0
- Bonus Lab 6 (Module 4) - Lab Reconfigure, HTTPS, LetsEncrypt
- Bonus Lab 7 (Module 7) - Configure Geo on Primary
- Bonus Lab 8 (Module 8) - Maintenance Mode 

### Reason for Bonus Labs
This course in particular is labs heavy, but with all the content in the slides and official labs, I still find there to be _not enough_ to cover the entire 16 hours duration of the course. The additional labs help out there as well as provide more advanced labs for some users to tackle.

## Additional Demos I came up with for course
Markdown instructions for [7 additional Bonus Demos](https://gitlab.com/demos-and-tools/gitlab-systems-administration-supplementary-content/-/blob/main/bonus-demos.MD)

- Bonus Demo 01 (Module 2) - Configure LetsEncrypt
- Bonus Demo 02 (Module 2) - Configure Postfix emails
- Bonus Demo 03 (Module 3) - ⁠Demo Backup & Restore Gitlab
- Bonus Demo 04 (Module 3) - Demo Upgrade Gitlab, upgrade path 
- Bonus Demo 05 (Module 3) - ⁠Demo Downgrade Gitlab
- Bonus Demo 06 (Module 7) - Demo Setup Geo, rake tasks, failover
- Bonus Demo 07 (Module 7) - Demo GEO Failover

### Reason for Bonus Demos
There are several demos in the official course, but I found the overall course content to be short in time so the additional demos helped demonstrate more of the product features to the students without needing to stretch out the time spent on existing content. Some of the demos I ended up not going through (like Geo Setup and Failover) but they are still good to have in my opinion as some classes will go faster than others (based on questions, discussions).

## Updated Slide Deck with speaker notes
Slide deck that I copied and made changes to (mostly speaker notes, outdated content, etc) :

[Link 1](https://docs.google.com/presentation/d/1EGFS5iDtRcGk-3wVstbP8aURioK4cdLBFQ-pBB8E-Fg), [Link 2](https://docs.google.com/presentation/d/1vmOyc8ssDJzjx9I94LSVzCj9DEeXacoyg7AOBbzTolg)

_Note : These links are Google Doc files that are privately shared with specific people_

### Change Suggestions to Slides
- Monitoring Module needs updates to remove embedded Grafana references, since that feature has been [deprecated](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/7772)
- Modules 5-8 needed fleshing out with speaker notes as well, most of the slides had no speaker notes
- Module 9 requires more slides and talking points, I added speaker notes to help. Otherwise this module does not have enough material in its current state. Using one of the bonus labs here may help as well
- Many of the DEMO sections required reverse engineering to figure out what to Demo, how much time to spend, and setting up projects to demo certain features


## LetsEncrypt Labs requiring custom DNS names
The EC2 instances come with AWS provided FQDN names but those DNS names are rejected by LetsEncrypt for SSL certification. As such during the last session I created ad-hoc DNS names for each instance. This allowed the students to practice enabling SSL via LetsEncrypt.


## Time Structure Planning

**Day 01 Time Table**

![image info](./timetable_day1.png)

**Day 02 Time Table**

![image info](./timetable_day2.png)
