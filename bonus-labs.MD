# Bonus Lab List

1. Bonus Lab 1 (Module 2) - Install Gitlab Manually
2. Bonus Lab 2 (Module 2) - Configure and test Postfix Email
3. Bonus Lab 3 (Module 2) - Lab use Docker executor with Runner
4. Bonus Lab 4 (Module 3) - ⁠Lab Upgrade Gitlab from 16 to 17.1
5. Bonus Lab 5 (Module 3) - ⁠Lab Downgrade Gitlab back to 16.0
6. Bonus Lab 6 (Module 4) - Lab Reconfigure, HTTPS, LetsEncrypt
7. Bonus Lab 7 (Module 7) - Configure Geo on Primary ([link](https://docs.gitlab.com/ee/administration/geo/setup/two_single_node_sites.html#set-up-geo-for-linux-package-omnibus "https://docs.gitlab.com/ee/administration/geo/setup/two_single_node_sites.html#set-up-geo-for-linux-package-omnibus"))
8. Bonus Lab 8 (Module 8) - Maintenance Mode 
  
### Download Pages for Gitlab

[https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-16.0.0-ee.0.el8.x86\_64.rpm](https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-16.0.0-ee.0.el8.x86_64.rpm)   
[https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-16.3.7-ee.0.el8.x86\_64.rpm](https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-16.3.7-ee.0.el8.x86_64.rpm)  
[https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-16.7.7-ee.0.el8.x86\_64.rpm](https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-16.7.7-ee.0.el8.x86_64.rpm)  
[https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-16.11.4-ee.0.el8.x86\_64.rpm](https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-16.11.4-ee.0.el8.x86_64.rpm)   
[https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-17.1.0-ee.0.el8.x86\_64.rpm](https://packages.gitlab.com/gitlab/gitlab-ee/packages/el/8/gitlab-ee-17.1.0-ee.0.el8.x86_64.rpm)  
  

### Bonus Lab 1 (Module 2) - Install Gitlab Manually
- Manual download Gitlab 16.0.0 installer here
- Install dependencies`sudo dnf install -y curl policycoreutils perl postfix   ``[sudo dnf install -y mailutils]`

- Install Gitlab 16.0.0
- Use HTTP IP or FQDN for External URL
- Configure with `⁠gitlab-ctl reconfigure`
- Grab root password from `/etc/gitlab/initial_root_password`⁠`⁠`
- ⁠Change root password, add a named user, disable signups
 

### Bonus Lab 2 (Module 2) - Configure and test Postfix Email

- Install dependencies`sudo dnf install -y curl policycoreutils perl postfix   ``[sudo dnf install -y mailutils]`⁠

- Start and enable postfix  
`sudo systemctl enable postfix`  
`sudo systemctl start postfix`⁠
- Add email enable line to gitlab.rb`gitlab_rails['gitlab_email_enabled'] = true`
- Re-configure with `⁠gitlab-ctl reconfigure`
- Test Email verification by adding an email to a user profile
- Troubleshoot Mail via `less /var/log/maillog`

  

### Bonus Lab 3 (Module 2) - Lab use Docker executor with Runner

- Uninstall podman if exists
- Install docker CE on [centos](https://computingforgeeks.com/install-docker-and-docker-compose-on-rhel-8-centos-8/ "https://computingforgeeks.com/install-docker-and-docker-compose-on-rhel-8-centos-8/") / [ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04 "https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04")  
`⁠sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo`⁠  
`sudo yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin`  

- Install Gitlab Runner with Docker executor  
`curl -LJO "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/rpm/gitlab-runner_amd64.rpm"`⁠  
`⁠curl -LJO "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/deb/gitlab-runner_amd64.deb"`  
- Register to Gitlab
    - Shared Runner
- Run CI pipeline
- Verify running in Docker

  

  

### Bonus Lab 4 (Module 3) - ⁠Lab Upgrade Gitlab from 16 to 17.1

- Use **Upgrade Path** to figure out hops
- Download Gitlab installers for each hop required
- **Backup Gitlab** before upgrading `⁠sudo gitlab-backup create   `
- Install Gitlab upgrade, one by one `⁠sudo yum install <installer-filename>`
- Verify and confirm Background Migrations complete before each hop

  

### Bonus Lab 5 (Module 3) - ⁠Lab Downgrade Gitlab back to 16.0

- Copy gitlab rb file to back it up`cp /etc/gitlab/gitlab.rb /etc/gitlab/gitlab.rb.bkup`
- Uninstall current version`sudo dnf remove gitlab-ee`
- Re-Install the original version 16.0.0`sudo yum install gitlab-ee-16.0.0-ee.0.el8.x86_64.rpm`
- Reconfigure to ensure gitlab.rb settings`sudo gitlab-ctl reconfigure`
- Restore Backup`ls /var/opt/gitlab/backups   ``sudo gitlab-backup restore BACKUP=filename-up-to-ee`
- Status Check`sudo gitlab-ctl status`
- Login and verify version is now on 16.0.0



### Bonus Lab 6 (Module 4) - Lab Reconfigure, HTTPS, LetsEncrypt

**Configure LetsEncrypt**  

- Create a custom DNS record pointing to your Gitlab server IP
- Configure LetsEncrypt options and use custom DNS record
    - Change external URL to use HTTPS fqdn in **gitlab.rb**  
`⁠external_url 'http://gitlabinstall2.dnsif.ca'`⁠  
    - Enable letsencrypt options in **gitlab.rb**
- Reconfigure and allow time for LetsEncrypt to issue certificates
- Troubleshoot for any issues
- Use Manual letsencrypt certification commands if needed
- Verify Gitlab serving through HTTPS

**Disable HTTP via NGINX config**

- Disable 80 on Nginx
- Edit **/etc/nginx/sites-available⁠**
- Comment out these two lines

```
 listen 80 default_server;
        listen [::]:80 default_server;
```
- Reload and restart nginx
- Confirm port 80 fails to load
    - Test with curl/wget
    - Test in a new browser session (incognito)



### Bonus Lab 7 (Module 7) - Configure Geo on Primary

Steps 1 through 12 of the “Configure the Primary Server” section

[https://docs.gitlab.com/ee/administration/geo/setup/two\_single\_node\_sites.html#configure-the-primary-site](https://docs.gitlab.com/ee/administration/geo/setup/two_single_node_sites.html#configure-the-primary-site)  



### Bonus Lab 8 (Module 8) - Maintenance Mode 

- Set up Maintenance mode [per here](https://docs.gitlab.com/ee/administration/maintenance_mode/#enable-maintenance-mode "https://docs.gitlab.com/ee/administration/maintenance_mode/#enable-maintenance-mode")
- Set up a Banner message
- Try to make some changes
    - Create an Issue
    - Create a Merge Request
    - Create a File
    - Create a new Project
- Disable Maintenance Mode